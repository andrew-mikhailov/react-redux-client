import React from 'react';
import {ConnectionStateContainer} from './ConnectionState/ConnectionState';
import './App.css';

export default React.createClass({
  render: function() {
    return <div>
      <ConnectionStateContainer />
      {this.props.children}
    </div>
  }
});
